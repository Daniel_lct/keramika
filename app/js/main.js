// Настройки меню

$(document).ready(function(){
    $('.mk-header__toggler').on('click', function(){
        // $('.mk-header__dropdown').fadeToggle()
        $('.mk-header__dropdown-wrapper').addClass('mk-header__dropdown-wrapper_active')
        setTimeout(function(){
            $('.mk-page').addClass('mk-page_active')
        },300)
    })

    $('.mk-header__dropdown-close button').on('click', function(){
        $('.mk-header__dropdown-wrapper').removeClass('mk-header__dropdown-wrapper_active')
        $('.mk-page').removeClass('mk-page_active')
    })
})


// Настройки строки поиска

$(document).ready(function(){
    $('.mk-search .mk-header__search-btn').on('click', function(){
        $('.mk-header__search-input').toggleClass('mk-header__search-input_active')

        $('.mk-header__search-btn').toggleClass('mk-header__search-btn_active')
    })
})


// Анимация кнопок на главной странице

$(document).ready(function(){
    $('.mk-services__link').each(function(){
        $(this).on('mouseover', function(){
            $(this).find('.mk-services__svg-static').hide()
            $(this).find('.mk-services__svg-active').show()
        })
    })

    $('.mk-services__link').each(function(){
        $(this).on('mouseout', function(){
            $(this).find('.mk-services__svg-static').show()
            $(this).find('.mk-services__svg-active').hide()
        })
    })
})


// Воспроизведение музыки

$(document).ready(function(){
    let a = document.querySelector('.sound-block')

    $('.sound-play').on('click', function(){
        a.play()
        $(this).hide()
        $('.sound-pause').show()
        $('.sound-circle').addClass('sound-circle_active')
    })

    $('.sound-pause').on('click', function(){
        a.pause()
        $('.sound-play').show()
        $(this).hide()
        $('.sound-circle').removeClass('sound-circle_active')

    })
})

// Конпка "Наверх"

var btn = $('#buttonToTop');

$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '300');
});


// Главный слайдер

var swiper = new Swiper('.swiper-container', {
    effect: 'fade',
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        320: {
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },
        },
        1200: {
            pagination: {
                el: '.swiper-pagination',
                type: 'progressbar',
            },
        }
    },
});


$(document).ready(function(){

    const openModalButtons = document.querySelectorAll('[data-modal-target]')
    const closeModalButtons = document.querySelectorAll('[data-close-button]')
    const overlay = document.getElementById('overlay');
    const overlayLightbox = document.getElementById('overlay-lightbox');


    openModalButtons.forEach(button => {
        button.addEventListener('click', (e) => {
            e.preventDefault()
            const modal = document.querySelector(button.dataset.modalTarget)
            openModal(modal)
        })
    })

    closeModalButtons.forEach(button => {
        button.addEventListener('click', (e) => {
            const modal = button.closest('.modal'); //select parent element html
            const modalLightbox = button.closest('.modal-lightbox'); //select parent element html


            closeModal(modal)
            closeModal(modalLightbox)

        })
    })

    overlay.addEventListener('click', () => {
        const modals = document.querySelectorAll('.modal.active')
        modals.forEach(modal => {
            closeModal(modal)
        })


    })

    document.querySelectorAll('.mk-design-card-slider .swiper-slide').forEach(i=>{
        i.addEventListener('click', function(){
            overlayLightbox.classList.add('active')
        })
    })



    function openModal(modal){
        // if(modal == null){
        //     return
        // }
        if(modal == null) return
        modal.classList.add('active')
        overlay.classList.add('active')
    }

    function closeModal(modal){
        // if(modal == null){
        //     return
        // }
        if(modal == null) return
        modal.classList.remove('active')
        overlay.classList.remove('active')
        overlayLightbox.classList.remove('active')
    }


})


// var galleryThumbs = new Swiper('.gallery-thumbs', {
//     spaceBetween: 10,
//     slidesPerView: 4,
//     freeMode: true,
//     watchSlidesVisibility: true,
//     watchSlidesProgress: true,
// });
// var galleryTop = new Swiper('.gallery-top', {
//     spaceBetween: 10,
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//     thumbs: {
//         swiper: galleryThumbs
//     }
// });

$(document).ready(function(){

    const thumbClass = 'thumblist-item';
    const mytap = window.ontouchstart === null?"touchstart":"click";

    const mySwiper = new Swiper('.mk-design-card-slider', {
        effect: 'fade',
        pagination:{
            el:'.thumblist',
            type:'custom',
            clickable: true,
            renderCustom: function (swiper, current, total) {
                const slides = swiper.slides;
                let html = '';
                for( let i = 0 ; i < total; i++ ){
                    if( current == i+1 ){
                        html = html + `<div class="swiper-pagination-bullet swiper-pagination-bullet-active" data-slideto="${i}">${slides[i].innerHTML}</div>`;
                    }else{
                        html = html + `<div class="swiper-pagination-bullet" data-slideto="${i}">${slides[i].innerHTML}</div>`;
                    }
                }
                return html;
            }
        },
    });

    const thumbClassLightBox = 'thumblist-item-lightbox';
    const mytapLightBox = window.ontouchstart === null?"touchstart":"click";

    const mySwiperLightBox = new Swiper('.mk-design-card-slider-lightbox', {
        effect: 'fade',
        pagination:{
            el:'.thumblist-lightbox',
            type:'custom',
            clickable: true,
            renderCustom: function (swiper, current, total) {
                const slides = swiper.slides;
                let html = '';
                for( let i = 0 ; i < total; i++ ){
                    if( current == i+1 ){
                        html = html + `<div class="swiper-pagination-bullet swiper-pagination-bullet-active" data-slideto="${i}">${slides[i].innerHTML}</div>`;
                    }else{
                        html = html + `<div class="swiper-pagination-bullet" data-slideto="${i}">${slides[i].innerHTML}</div>`;
                    }
                }
                return html;
            }
        },
    });

})


let rotate = 30

swiper.on('slideChange', function(){
    $('.slide-active').text('0' + (swiper.realIndex+1))
    $('.num-active').text('0' + (swiper.realIndex+1))
    $('.mk-star').css({'transform' : 'rotate('+ rotate +'deg)'})
    rotate = rotate + 30
})

$('.slide-active').text('0' + (swiper.realIndex+1))
$('.num-active').text('0' + (swiper.realIndex+1))

$(document).ready(function(){
    $('.num-length').text('0' + $('.mk-main-screen__slide').length)
})

$(document).ready(function () {
    $('.mk-design-bathroom__nav-item a').each(function(){
        $(this).hover(function(){
            $('.mk-design-bathroom__item-hover img').attr('src', `img/${$(this).data('type')}.png`)
        })
    })
})